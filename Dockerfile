FROM ruby:2.5.3-alpine3.8

RUN apk add --no-cache --update build-base \
                                linux-headers \
                                git \
                                mariadb-dev \
                                nodejs \
                                tzdata


ENV APP_PATH /rails-api

RUN mkdir $APP_PATH
WORKDIR $APP_PATH

ADD Gemfile $APP_PATH
ADD Gemfile.lock $APP_PATH
RUN bundle install

COPY . APP_PATH
EXPOSE 4000