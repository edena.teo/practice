require 'test_helper'

class MeepsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @meep = meeps(:one)
  end

  test "should get index" do
    get meeps_url, as: :json
    assert_response :success
  end

  test "should create meep" do
    assert_difference('Meep.count') do
      post meeps_url, params: { meep: { description: @meep.description, name: @meep.name } }, as: :json
    end

    assert_response 201
  end

  test "should show meep" do
    get meep_url(@meep), as: :json
    assert_response :success
  end

  test "should update meep" do
    patch meep_url(@meep), params: { meep: { description: @meep.description, name: @meep.name } }, as: :json
    assert_response 200
  end

  test "should destroy meep" do
    assert_difference('Meep.count', -1) do
      delete meep_url(@meep), as: :json
    end

    assert_response 204
  end
end
