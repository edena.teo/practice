Rails.application.routes.draw do
  resources :meeps
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    resources :todos
    # resources :base, except: [:index]
    # resources :base, only: [:index]
    # root :to => 'home#index'
    # get "/home", to: "home#index"
  end
end
