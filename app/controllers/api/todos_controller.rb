class Api::TodosController < ApplicationController 
    def index
        render json: Todo.all
    end
     
    #get
    def show
        render json: Todo.find(params[:id])
        # render json: Todo.where(id:params[:id])
    end

    #post
    def create
        todo = Todo.new(request_params)

        #use unless cos it's more efficient
        unless todo.save
            return render json: {error: todo.errors}
        end
        render json: todo

        # if todo.save
        #     render json: todo
        # else
        #     return render json: {error: todo.errors}
        # end
    end

    #put/patch (what's diff?)
    def update
        todo = Todo.find(params[:id])
        # todo.name = request_params[:name]
        # todo.save
        # render json: todo
        render json: todo.update_attributes(request_params)
    end

    #delete
    def destroy
        todo = Todo.find(params[:id])
        render json todo.destroy
    end

    private

    #strong parameters
    #requires parameters to have todo key and only allows name and description
    def request_params
        params.require(:todo).permit(:name, :description)
    end
end
