class MeepsController < ApplicationController
  before_action :set_meep, only: [:show, :update, :destroy]

  # GET /meeps
  def index
    @meeps = Meep.all

    render json: @meeps
  end

  # GET /meeps/1
  def show
    render json: @meep
  end

  # POST /meeps
  def create
    @meep = Meep.new(meep_params)

    if @meep.save
      render json: @meep, status: :created, location: @meep
    else
      render json: @meep.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /meeps/1
  def update
    if @meep.update(meep_params)
      render json: @meep
    else
      render json: @meep.errors, status: :unprocessable_entity
    end
  end

  # DELETE /meeps/1
  def destroy
    @meep.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meep
      @meep = Meep.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def meep_params
      params.require(:meep).permit(:name, :description)
    end
end
